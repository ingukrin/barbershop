package com.barbershop.entity;

import javax.persistence.*;

@Entity
@Table(name="customer")
//@org.hibernate.annotations.NamedQuery(name="Get",
//        query = "from customer where CustomerId = :id")

public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CustomerId")
    private Integer id;

    @Column(name = "CustName")
    private String custName;

    @Column(name = "CustPhone")
    private String custPhone;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getCustPhone() {
        return custPhone;
    }

    public void setCustPhone(String custPhone) {
        this.custPhone = custPhone;
    }

    public Customer() {
    }

    public Customer(String custPhone, String custName) {
        this.custName = custName;
        this.custPhone = custPhone;
    }

    @Override
    public String toString() {
        return "CustomerEnt{" +
                "id=" + id +
                ", custName='" + custName + '\'' +
                ", custPhone='" + custPhone + '\'' +
                '}';
    }




}
