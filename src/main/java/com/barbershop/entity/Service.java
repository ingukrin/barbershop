package com.barbershop.entity;


import org.hibernate.annotations.Type;
import org.hibernate.mapping.Set;

import javax.persistence.*;

@Entity
@Table(name="service")


public class Service {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ServiceId")
    private Integer id;

    @Column(name = "ServiceName")
    private String serviceName;

    @Column(name = "ServicePrice")
    private double servicePrice;

    @Column(nullable = false, columnDefinition = "TINYINT(1)",name = "Valid")
    @Type(type = "org.hibernate.type.NumericBooleanType")
    private boolean valid;

//    @OneToMany(cascade = {CascadeType.ALL},mappedBy = "serviceid")
//    private Set<SheduleEnt> sheduleEnt;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public double getServicePrice() {
        return servicePrice;
    }

    public void setServicePrice(double servicePrice) {
        this.servicePrice = servicePrice;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

//    public Set<SheduleEnt> getSheduleEnt() {
//        return sheduleEnt;
//    }
//
//    public void setSheduleEnt(Set<SheduleEnt> sheduleEnt) {
//        this.sheduleEnt = sheduleEnt;
//    }
}
