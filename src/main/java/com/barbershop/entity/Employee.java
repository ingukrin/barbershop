package com.barbershop.entity;

import org.hibernate.annotations.Type;
import org.hibernate.mapping.Set;

import javax.persistence.*;

@Entity
@Table(name="employee")
//@Table(name="EMPLOYEE")

public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "EmpId")
    private Integer id;

    @Column(name = "EmpPhone")
    private String empPhone;

    @Column(name = "EmpPosition")
    private String empPosition;

    @Column(name = "EmpName")
    private String empName;



    @Column(nullable = false, columnDefinition = "TINYINT(1)", name = "Valid")
    @Type(type = "org.hibernate.type.NumericBooleanType")
    private boolean valid;

//    @OneToMany(cascade = {CascadeType.ALL},mappedBy = "emplId")
//    private Set<SheduleEnt> sheduleEnt;

    public Employee() {
        this.valid=true;
    }

    public Employee(String empPhone, String empPosition, String empName) {
        this.empPhone = empPhone;
        this.empPosition = empPosition;
        this.empName = empName;
        this.valid=true;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmpPhone() {
        return empPhone;
    }

    public void setEmpPhone(String empPhone) {
        this.empPhone = empPhone;
    }

    public String getEmpPosition() {
        return empPosition;
    }

    public void setEmpPosition(String empPosition) {
        this.empPosition = empPosition;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

//
//    public Set<SheduleEnt> getSheduleEnt() {
//        return sheduleEnt;
//    }
//
//    public void setSheduleEnt(Set<SheduleEnt> sheduleEnt) {
//        this.sheduleEnt = sheduleEnt;
//    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", empPhone='" + empPhone + '\'' +
                ", empPosition='" + empPosition + '\'' +
                ", empName='" + empName + '\'' +
                ", valid=" + valid +
                '}';
    }
}
