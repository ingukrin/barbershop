package com.barbershop.entity;



import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="shedule")


public class Shedule {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idShedule")
    private Integer id;

//    @ManyToOne //(fetch = FetchType.EAGER)
//    @JoinColumn(name = "EmplId")
    @Column(name = "EmplId")
    private Integer emplId;

    //@ManyToOne //(fetch = FetchType.EAGER)
    //@JoinColumn(name = "Serviceid")
    @Column(name = "Serviceid")
    private Integer serviceid;

    //@ManyToOne (fetch = FetchType.EAGER)
   //  @JoinColumn(name = "CutomerId")
    @Column(name = "CutomerId")
    private Integer cutomerId;

    @Column(name = "StartTime")
    private Date startTime;

    @Column(name = "EndTime")
    private Date endTime;

    @Column(nullable = false, columnDefinition = "TINYINT(1)", name = "Canceled")
    @Type(type = "org.hibernate.type.NumericBooleanType")
    private boolean canceled;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getEmplId() {
        return emplId;
    }

    public void setEmplId(Integer emplId) {
        this.emplId = emplId;
    }

    public Integer getServiceid() {
        return serviceid;
    }

    public void setServiceid(Integer serviceid) {
        this.serviceid = serviceid;
    }

    public Integer getCutomerId() {
        return cutomerId;
    }

    public void setCutomerId(Integer cutomerId) {
        this.cutomerId = cutomerId;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public boolean isCanceled() {
        return canceled;
    }

    public void setCanceled(boolean canceled) {
        this.canceled = canceled;
    }

    @Override
    public String toString() {
        return "Shedule{" +
                "id=" + id +
                ", emplId=" + emplId +
                ", serviceid=" + serviceid +
                ", cutomerId=" + cutomerId +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", canceled=" + canceled +
                '}';
    }
}
