package com.barbershop;

import com.barbershop.hibernateProcess.ProcesEmployee;
import com.barbershop.entity.Employee;
import com.barbershop.hibernateProcess.ProcesServices;
import com.barbershop.ui.ui;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        //menuMain();
        ProcesServices.loadAllServices();
        ui.clearScreen();
        ui.setMenu(1);
        ui.printMenu();
        ui.makeChoise();

        do {
            ui.clearScreen();
            ui.printMenu();
            ui.makeChoise();
            if (ui.getMenu()==-100)
            {
                break;
            }
        }while (true);

    }

}
