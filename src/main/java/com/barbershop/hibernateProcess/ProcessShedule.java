package com.barbershop.hibernateProcess;
import com.barbershop.entity.Customer;
import com.barbershop.entity.Shedule;
import com.barbershop.ui.ui;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class ProcessShedule {

    public static List<Shedule> sheduleList;

    public static void loadAllShedule(){

        EntityManager entmen;
        Session session = HibernateUtil.getSessionFactory().openSession();
        entmen = session.getEntityManagerFactory().createEntityManager();
        session.beginTransaction();
        Query query = session.createQuery("From Shedule", Shedule.class);
        List<Shedule> list = query.getResultList();
        session.getTransaction().commit();
        session.close();
        ProcessShedule.sheduleList= list;
    }

    public static List<Shedule> loadAllSheduleCuitomer(Integer customerId){
        EntityManager entmen;
        List<Shedule> list =null;
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            entmen = session.getEntityManagerFactory().createEntityManager();
            session.beginTransaction();
            Query query = session.createQuery("From Shedule Where CutomerId=:customerId", Shedule.class);
            query.setParameter("customerId", customerId);
            list = query.getResultList();
            session.getTransaction().commit();
            session.close();
        }catch (Exception e){
            System.err.println(e);
        }
        return list;
    }

    public static void prinShudeleForCutomer(Integer customerId){
        List<Shedule> list =loadAllSheduleCuitomer(customerId);
        ui.clearScreen();
        System.out.println("RezNr\tEmployeeName\tRezervationTime\tService_name\tService_Price");
        try {
            for (Shedule a: list){
                System.out.println(a.getId()+"\t\t"
                        +ProcesEmployee.getEmpName(a.getEmplId())+"\t"
                        +a.getStartTime()+"\t"
                        +ProcesServices.getServicename(a.getServiceid())+ "\t"
                        +ProcesServices.getServicePrice(a.getServiceid()));
            }
        }

        catch (NullPointerException x){
            System.out.println("No data found");
        }
        catch (Exception e){
            System.out.println(e);
        }
        System.out.println("Press Enter to continue");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            String option = reader.readLine();
        }

        catch (Exception e)
        {
            System.out.println(e);
        }

    }

    public static List<Shedule> loadAllSheduleEmplyee(Integer emplID) {
        EntityManager entmen;
        List<Shedule> list =null;
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            entmen = session.getEntityManagerFactory().createEntityManager();
            session.beginTransaction();
            Query query = session.createQuery("From Shedule Where EmplId=:emplID", Shedule.class);
            query.setParameter("emplID", emplID);
            list = query.getResultList();
            session.getTransaction().commit();
            session.close();
        }catch (Exception e){
            System.err.println(e);
        }
        return list;
    }

    public static void prinShudeleForEmployee(Integer emplID){
        List<Shedule> list =loadAllSheduleEmplyee(emplID);
        ui.clearScreen();
        System.out.println("RezNr\tEmployeeName\tRezervationTime\tService_name\tService_Price");
        try {
            for (Shedule a: list){
                System.out.println(a.getId()+"\t\t"
                        +ProcesEmployee.getEmpName(a.getEmplId())+"\t"
                        +a.getStartTime()+"\t"
                        +ProcesServices.getServicename(a.getServiceid())+ "\t"
                        +ProcesServices.getServicePrice(a.getServiceid()));
            }
        }

        catch (NullPointerException x){
            System.out.println("No data found");
        }
        catch (Exception e){
            System.out.println(e);
        }
        System.out.println("Press Enter to continue");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            String option = reader.readLine();
        }
        catch (Exception e)
        {
            System.out.println(e);
        }
    }



    public static List<Shedule> loadFreeShedule(){
        EntityManager entmen;
        Session session = HibernateUtil.getSessionFactory().openSession();
        entmen = session.getEntityManagerFactory().createEntityManager();
        session.beginTransaction();
        Query query = session.createQuery("From Shedule Where CutomerId='1'", Shedule.class);
        List<Shedule> list = query.getResultList();
        session.getTransaction().commit();
        session.close();
        return list;
    }

    public static void saveShedule(Shedule she){
        EntityManager entmen;
        Session session = HibernateUtil.getSessionFactory().openSession();
        entmen = session.getEntityManagerFactory().createEntityManager();
        session.beginTransaction();
        session.update(she);
        session.getTransaction().commit();
        session.close();
    }

    public static void getRezervationDates(Date rezDate){
        ProcesServices.loadAllServices();
    };

}
