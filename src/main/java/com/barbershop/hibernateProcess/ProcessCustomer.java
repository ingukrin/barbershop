package com.barbershop.hibernateProcess;
import com.barbershop.entity.Customer;
import com.barbershop.entity.Employee;
import org.hibernate.Session;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;



public class ProcessCustomer {

    public static Customer cutomer;

    public static List<Customer> getAllCustomers(){
        EntityManager entmen;
        Session session = HibernateUtil.getSessionFactory().openSession();
        entmen = session.getEntityManagerFactory().createEntityManager();
        session.beginTransaction();
        Query query = session.createQuery("From Customer", Customer.class);
        List<Customer> list = query.getResultList();
        //    System.out.println(list);
        session.getTransaction().commit();
        session.close();
        return  list;
    }

    public static void addCustomer(String name,  String phone){
        EntityManager entmen;
        Session session = HibernateUtil.getSessionFactory().openSession();
        entmen = session.getEntityManagerFactory().createEntityManager();
        Customer customer = new Customer( phone, name );
        session.save(customer);
        session.beginTransaction();
        session.save(customer);
        session.getTransaction().commit();
        session.close();
        ProcessCustomer.cutomer=customer;
    }



    public static Customer findCutomerByPhone(String phone){
        EntityManager entmen;
        Session session = HibernateUtil.getSessionFactory().openSession();
        entmen = session.getEntityManagerFactory().createEntityManager();
        session.beginTransaction();
        Query query = session.createQuery("From Customer Where CustPhone=:phone", Customer.class);
        query.setParameter("phone",phone);
        List<Customer> list = query.getResultList();
        session.getTransaction().commit();
        session.close();
        return  list.get(0);
    }




    public static void printCustomer(){
        //System.out.println("Customer-->"+ProcessCustomer.cutomer);
        if(ProcessCustomer.cutomer == null){
            System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXX");
            System.out.println("Customer is not inicialayzed ");
            System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXX");
        }
        else{
            System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXX");
            System.out.println("Customer name is " + cutomer.getCustName()+ "  and phone "+cutomer.getCustPhone()+".");
            System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXX");
        }
    }

    public static boolean isCutomerInivialyzed(){
        return ProcessCustomer.cutomer!= null;
    }


}
