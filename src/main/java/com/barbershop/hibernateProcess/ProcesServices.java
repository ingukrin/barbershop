package com.barbershop.hibernateProcess;

import com.barbershop.entity.Customer;
import com.barbershop.entity.Service;
import org.hibernate.Session;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

public class ProcesServices {

    public static List<Service> servList;

    public static void loadAllServices(){

        EntityManager entmen;
        Session session = HibernateUtil.getSessionFactory().openSession();
        entmen = session.getEntityManagerFactory().createEntityManager();
        session.beginTransaction();
        Query query = session.createQuery("From Service", Service.class);
        List<Service> list = query.getResultList();
        session.getTransaction().commit();
        session.close();
        ProcesServices.servList= list;
    }




}
